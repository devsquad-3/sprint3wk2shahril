package jdbc01.dao01;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import jdbc01.pojo01.Customer;
import utility01.DBUtility01;
import jdbc01.service01.CustomerServiceImpl;

public class CustomerDAOImpl implements CustomerDAO{
	
	Connection refConnection=null;
	PreparedStatement refPreparedStatement =null;
	
	@Override
	public void closeConnection(Customer refCustomer) {
		//System.out.println("Logging out");
		refCustomer = null;
		try {
			refConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			System.out.println("Closing Connection..");
			System.exit(0);
		}
		
		
	}

	@Override
	public boolean CustomerLogin(Customer refCustomer) {
		try {
			refConnection = DBUtility01.getConnection();
			
			String sqlQuery = "select CustomerID,CustomerPassword from customer where CustomerID=?";
			
			
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			System.out.println();
			
			refPreparedStatement.setString(1, refCustomer.getCustomerID());
			ResultSet rs=refPreparedStatement.executeQuery();
			rs.next();
			if (rs.getString("CustomerID").equals(refCustomer.getCustomerID()) &&(rs.getString("CustomerPassword").equals(refCustomer.getCustomerPassword()))) {
				System.out.println("Login Succesfull");
				return true;
			}
		
		} catch (SQLException e) {
			System.out.println("Login Failed");
			
		}
		return false;
	}
}