package jdbc01.service01;

public interface CustomerService {
	
	void customerChoice();
	void customerAuthentication();
	void customerCloseConnection();

}
