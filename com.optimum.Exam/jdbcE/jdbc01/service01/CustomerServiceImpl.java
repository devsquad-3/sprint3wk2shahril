package jdbc01.service01;

import java.util.Scanner;

import jdbc01.dao01.CustomerDAO;
import jdbc01.dao01.CustomerDAOImpl;
import jdbc01.pojo01.Customer;

public class CustomerServiceImpl implements CustomerService{
	
	CustomerDAO refCustomerDAO;
	Scanner refScanner;
	Customer refCustomer;
	
	@Override
	public void customerChoice() {
		var showMenu=true;
		while(showMenu==true) {
			
			System.out.println("What would you like to do?");
			System.out.println("Enter Choice");
			System.out.println("1.Change Username 2.Change Password 3.Logout");
			

			refScanner = new Scanner(System.in);
			int choice = refScanner.nextInt();
			switch (choice) {
			case 1:
				System.out.println("Sike didnt program this");
				
				break;
			case 2:
				System.out.println("Sike didnt program this either hehehe");
				break;
			case 3:
				customerCloseConnection();
				showMenu=false;
				
				
				break;
			case 4:
				break;
			case 5:
				
				break;
			default:
				System.out.println("Option not found..");
				break;

			}
		}
		
	}

	@Override
	public void customerAuthentication() {
		refScanner = new Scanner(System.in);

		System.out.println("Enter Customer Name:");
		String customerId = refScanner.next();
		System.out.println("Enter Password:");
		String customerPassword = refScanner.next();
		
		
		refCustomer = new Customer();
		refCustomer.setCustomerID(customerId);
		refCustomer.setCustomerPassword(customerPassword);
		
		
		refCustomerDAO = new CustomerDAOImpl();
		
		if (refCustomerDAO.CustomerLogin(refCustomer)) {
			customerChoice();
		} else {
			//System.out.println("Logging out");
			System.out.println("Closing Connection..");
			System.out.println("\nEnd");
			
		}
		
		
	}

	@Override
	public void customerCloseConnection() {
		refCustomerDAO = new CustomerDAOImpl();
		refCustomerDAO.closeConnection(refCustomer);
		
		
	}

}