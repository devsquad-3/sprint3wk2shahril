package jdbc01.application01;

import jdbc01.controller01.CustomerController;
import jdbc01.dao01.CustomerDAOImpl;
import utility01.DBUtility01;

public class CustomerApplication {

	public static void main(String[] args) {
		
		CustomerController refCustomerController = new CustomerController();
		refCustomerController.getCustomerService();

	}

}
